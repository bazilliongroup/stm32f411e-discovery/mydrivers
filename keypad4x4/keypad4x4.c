/*
Library:			        4x4 Keypad drive for STM32 MCUs
Written by:				Mohamed Yaqoob
Date written:				03/04/2018
Description:				The keypad4x4 library consists of the following public functions
										Function(1)- Keypad4x4_Init
										Function(2)- Keypad4x4_ReadKeypad
										Function(3)- Keypad4x4_GetChar

                                       The 3X4 represents 3 columns X 4 rows modified for the 3x4 Matrix Membrane Keypad
                                        I recommend you build it manually and do the modifications. Here is the logic matrix.

                                        PINS (8-5)rows; (4-1)cols.: Pins 8,7,6,5,4 should have a pull down resistor, I used 4.7K Ohms
                                        row1 = pin8, row2 = pin7, row3 = pin6, row4 = pin5
                                        col1 = pin4, col2 = pin3, col3 = pin2, col4 = pin1

                                       |  rows | cols  |
                                       |8,7,6,5|4,3,2,1| <- PINS
                                       --------|-------------------
                                       [1,2,3,4|1,2,3,4] <- ROW/COL
                                       ----------------------------
                                        1,0,0,0,1,0,0,0	   '1'
                                        1,0,0,0,0,1,0,0	   '2'
                                        1,0,0,0,0,0,1,0	   '3'
                                        1,0,0,0,0,0,0,1	   'A'
                                        0,1,0,0,1,0,0,0	   '4'
                                        0,1,0,0,0,1,0,0	   '5'
                                        0,1,0,0,0,0,1,0	   '6'
                                        0,1,0,0,0,0,0,1	   'B'
                                        0,0,1,0,1,0,0,0	   '7'
                                        0,0,1,0,0,1,0,0	   '8'
                                        0,0,1,0,0,0,1,0	   '9'
                                        0,0,1,0,0,0,0,1	   'C'
                                        0,0,0,1,1,0,0,0	   '*'
                                        0,0,0,1,0,1,0,0	   '0'
                                        0,0,0,1,0,0,1,0	   '#'
                                        0,0,0,1,0,0,0,1	   'D'
            
                                        The way the keypad algorithm works is you set a column high and check each row to see if a key was pressed. For example if number 3 is pressed
                                        col3 will high high and so will row1. Each number/letter has a code of a combination of rows and columns. This code goes from left to right 
                                        with rows and columns. So 3 is row1/col3.

Resources:
                                        https://www.jameco.com/webapp/wcs/stores/servlet/ProductDisplay?langId=-1&storeId=10001&productId=2246338&catalogId=10001&CID=CATJAN218PDF
                                        https://www.youtube.com/watch?v=vcxz2Dhx5WY&list=PLfExI9i0v1sn_lQjCFJHrDSpvZ8F2CpkA&index=23
                                        http://hertaville.com/stm32f0-gpio-tutorial-part-1.html
Modifications:
-----------------
Ronald Bazillion 	09/03/2018      Refactored original code 
*/

//***** Header files *****//
#include "stm32f7xx_hal.h"
#include "keypad4x4.h"

//*******Defines***************//
//#define KEYPAD_TEST

//*******Enums*****************//

//***** Constant variables and typedefs *****//

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//
//1. Keypad pin out variable (keypad library)
static Keypad_WiresTypeDef m_KeypadStruct;
//2. OUT pins position, or pin number in decimal for use in colomn change function
static uint8_t m_OutPositions[4];

static ACTIVE_COLUMN m_ActiveColumn;              //variable to store active column when searching.
static bool m_RotateRowsCols;                     //option used in case the 4x4 keypad matrix is reversed.

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//

//Function(1): Change column number
static void m_Keypad4x4_ChangeColumn(ACTIVE_COLUMN colNum);

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

//Function(1): Set Keypad pins and ports, rotate is used in case the keypad matrix is reversed.
//The defeault setting "false" has (rows & columns) while "true" has (colums & rows). 
void Keypad4x4_Init(Keypad_WiresTypeDef  *KeypadWiringStruct, bool rotate)
{  
	//Step(1): Copy the Keypad wirings to the library
	m_KeypadStruct = *KeypadWiringStruct;
        
        m_RotateRowsCols = rotate;
                
        //Step(2): Find the positions of the 4 OUT pins
	//Scroll through OUTXpin value and record the position.
	//EX. OUT0pin = GPIO_PIN_1 = ((uint16_t)0x0002) scroll until the bit is found and record as idx
	for(uint8_t idx = 0; idx < NUM_OF_REG; idx++)
	{
		//find the position
		if(((m_KeypadStruct.OUT0pin>>idx)&0x0001) == 0x0001)
		{
			m_OutPositions[0] = idx;
		}

		if(((m_KeypadStruct.OUT1pin>>idx)&0x0001) == 0x0001)
		{
			m_OutPositions[1] = idx;
		}

		if(((m_KeypadStruct.OUT2pin>>idx)&0x0001) == 0x0001)
		{
			m_OutPositions[2] = idx;
		}
                
                if(((m_KeypadStruct.OUT3pin>>idx)&0x0001) == 0x0001)
		{
			m_OutPositions[3] = idx;
		}
	}
        
	//Step(3): Initialise all pins to set all OUT pins to RESET
	m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
	m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
	m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
	m_KeypadStruct.OUT3_Port->OTYPER |= (1UL << m_OutPositions[3]);
	
	HAL_GPIO_WritePin(m_KeypadStruct.OUT0_Port, m_KeypadStruct.OUT0pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(m_KeypadStruct.OUT1_Port, m_KeypadStruct.OUT1pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(m_KeypadStruct.OUT2_Port, m_KeypadStruct.OUT2pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(m_KeypadStruct.OUT3_Port, m_KeypadStruct.OUT3pin, GPIO_PIN_SET);
        
#ifdef KEYPAD_TEST
        m_KeypadStruct.OUT0_Port->OTYPER &= ~(1UL << m_OutPositions[0]);	//Setting Column 1 high
        m_KeypadStruct.OUT1_Port->OTYPER &= ~(1UL << m_OutPositions[1]);	//Setting Column 2 high
        m_KeypadStruct.OUT2_Port->OTYPER &= ~(1UL << m_OutPositions[2]);	//Setting Column 3 high
        m_KeypadStruct.OUT3_Port->OTYPER &= ~(1UL << m_OutPositions[3]);	//Setting Column 4 high
#endif        
 }

//Function(2): Read active keypad button
//NOTE: with this function the Row ports Pins8-5 are configured to be resistor pull down.
//      This can be done in the software or done with actual pull down resistors.
//      I originally had it not returning but with arduino keypads it returned false row values
//      so I return upon the first keypress found from this function. Through testing it worked fine.
KEYNUM Keypad4x4_ReadKeypad(void)
{
        //Step(1): Make Pin4 High and check the rows
	m_Keypad4x4_ChangeColumn(COL1);
        if (m_ActiveColumn == COL1)
        {
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN0_Port, m_KeypadStruct.IN0pin) == GPIO_PIN_SET) { return KEY1; }	//Pin4 and Pin8
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN1_Port, m_KeypadStruct.IN1pin) == GPIO_PIN_SET) { return KEY4; }	//Pin4 and Pin7
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN2_Port, m_KeypadStruct.IN2pin) == GPIO_PIN_SET) { return KEY7; }	//Pin4 and Row6
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN3_Port, m_KeypadStruct.IN3pin) == GPIO_PIN_SET) { return KEY_STAR; } 	//Pin4 and Row5
        }
          
	//Step(2): Make Pin3 High and check the rows
	m_Keypad4x4_ChangeColumn(COL2);
        if (m_ActiveColumn == COL2)
        {
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN0_Port, m_KeypadStruct.IN0pin) == GPIO_PIN_SET) { return KEY2; } //Pin3 and Pin8
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN1_Port, m_KeypadStruct.IN1pin) == GPIO_PIN_SET) { return KEY5; } //Pin3 and Pin7
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN2_Port, m_KeypadStruct.IN2pin) == GPIO_PIN_SET) { return KEY8; } //Pin3 and Pin6
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN3_Port, m_KeypadStruct.IN3pin) == GPIO_PIN_SET) { return KEY0; } //Pin3 and Pin5
        }
                
	//Step(3): Make Pin2 High and check the rows
	m_Keypad4x4_ChangeColumn(COL3);
        if (m_ActiveColumn == COL3)
        {
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN0_Port, m_KeypadStruct.IN0pin) == GPIO_PIN_SET) { return KEY3; }      //Pin2 and Pin8
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN1_Port, m_KeypadStruct.IN1pin) == GPIO_PIN_SET) { return KEY6; }      //Pin2 and Pin7
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN2_Port, m_KeypadStruct.IN2pin) == GPIO_PIN_SET) { return KEY9; }      //Pin2 and Pin6
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN3_Port, m_KeypadStruct.IN3pin) == GPIO_PIN_SET) { return KEY_POUND; } //Pin2 and Pin5
        }    
        
        //Step(4): Make Pin1 High and check the rows
	m_Keypad4x4_ChangeColumn(COL4);
        if (m_ActiveColumn == COL4)
        {
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN0_Port, m_KeypadStruct.IN0pin) == GPIO_PIN_SET) { return KEY_A; }     //Pin1 and Pin8
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN1_Port, m_KeypadStruct.IN1pin) == GPIO_PIN_SET) { return KEY_B; }     //Pin1 and Pin7
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN2_Port, m_KeypadStruct.IN2pin) == GPIO_PIN_SET) { return KEY_C; }     //Pin1 and Pin6
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN3_Port, m_KeypadStruct.IN3pin) == GPIO_PIN_SET) { return KEY_D; }     //Pin1 and Pin5
        }     
                
        //Revert cols back to default
        m_Keypad4x4_ChangeColumn(NO_COL);
	return KBD_INVALID;
}	

//Function(3): Get character. If m_RotateRowsCols is true is returns the values as if the rows/cols were switched.
//Some keypads have the matrix reversed.
char Keypad4x4_GetChar(KEYNUM key)
{        
	switch (key)
	{
	case KEY1:
          if (m_RotateRowsCols) { return '1'; }
          else { return '1'; }
	  break;
	case KEY2:
          if (m_RotateRowsCols) { return '4';}
          else { return '2'; }
          break;
	case KEY3:
          if (m_RotateRowsCols) { return  '7';}
	  else { return '3'; }
          break;
        case KEY_A:
          if (m_RotateRowsCols) { return '*'; }
          else { return 'A'; }
	  break;    
	case KEY4:
          if (m_RotateRowsCols) { return '2'; }
	  else { return '4'; }
	  break;
	case KEY5:
          if (m_RotateRowsCols) { return '5'; }
	  else { return '5'; }
	  break;
	case KEY6:
          if (m_RotateRowsCols) { return '8'; }
	  else { return '6'; }
	  break;
        case KEY_B:
          if (m_RotateRowsCols) { return '0'; }
	  else { return 'B'; }
	  break;                
	case KEY7:
          if (m_RotateRowsCols) { return '3'; } 
	  else { return '7'; }
	  break;
	case KEY8:
          if (m_RotateRowsCols) { return '6'; }
	  else { return '8'; }
	  break;
	case KEY9:
          if (m_RotateRowsCols) { return '9'; }
	  else { return '9'; }
	  break;
        case KEY_C:
          if (m_RotateRowsCols) { return '#'; }
          else { return 'C'; }
	  break;                
	case KEY_STAR:
          if (m_RotateRowsCols) { return 'A'; }
	  else { return '*'; }
	  break;
	case KEY0:
          if (m_RotateRowsCols) { return 'B'; }
	  else { return '0'; }
	  break;
	case KEY_POUND:
          if (m_RotateRowsCols) { return 'C'; }
          else { return '#'; }
	  break;
        case KEY_D:
          if (m_RotateRowsCols) { return 'D'; }
	  else { return 'D'; }
	  break;                              
	}
        
	return NULL;
}

//Function(4): change character to constant character
//Used in debugging if you need to have data send as text.
const char* Keypad4x4_GetConstChar(KEYNUM key)
{
	switch (key)
	{
	case KEY1:
          if (m_RotateRowsCols) { return "KEY_1 "; }
          else { return "KEY_1 "; }
	  break;
	case KEY2:
          if (m_RotateRowsCols) { return "KEY_4 ";}
          else { return "KEY_2 "; }
          break;
	case KEY3:
          if (m_RotateRowsCols) { return  "KEY_7 ";}
	  else { return "KEY_3 "; }
          break;
        case KEY_A:
          if (m_RotateRowsCols) { return "KEY_* "; }
          else { return "KEY_A "; }
	  break;    
	case KEY4:
          if (m_RotateRowsCols) { return "KEY_2 "; }
	  else { return "KEY_4 "; }
	  break;
	case KEY5:
          if (m_RotateRowsCols) { return "KEY_5 "; }
	  else { return "KEY_5 "; }
	  break;
	case KEY6:
          if (m_RotateRowsCols) { return "KEY_8 "; }
	  else { return "KEY_6 "; }
	  break;
        case KEY_B:
          if (m_RotateRowsCols) { return "KEY_0 "; }
	  else { return "KEY_B "; }
	  break;                
	case KEY7:
          if (m_RotateRowsCols) { return "KEY_3 "; } 
	  else { return "KEY_7 "; }
	  break;
	case KEY8:
          if (m_RotateRowsCols) { return "KEY_6 "; }
	  else { return "KEY_8 "; }
	  break;
	case KEY9:
          if (m_RotateRowsCols) { return "KEY_9 "; }
	  else { return "KEY_9 "; }
	  break;
        case KEY_C:
          if (m_RotateRowsCols) { return "KEY_# "; }
          else { return "KEY_C "; }
	  break;                
	case KEY_STAR:
          if (m_RotateRowsCols) { return "KEY_A "; }
	  else { return "KEY_* "; }
	  break;
	case KEY0:
          if (m_RotateRowsCols) { return "KEY_B "; }
	  else { return "KEY_0 "; }
	  break;
	case KEY_POUND:
          if (m_RotateRowsCols) { return "KEY_C "; }
          else { return "KEY_# "; }
	  break;
        case KEY_D:
          if (m_RotateRowsCols) { return "KEY_D "; }
	  else { return "KEY_D "; }
	  break;                              
	}
        
	return NULL;  
}

// Change column number.
//It does this by changing the state of the type register from open-drain to Output Push Pull.
static void m_Keypad4x4_ChangeColumn(ACTIVE_COLUMN colNum)
{
	if(colNum == COL1)
	{
		//Set selected column, and make other columns floating
		m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
		m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
                m_KeypadStruct.OUT3_Port->OTYPER |= (1UL << m_OutPositions[3]);
		m_KeypadStruct.OUT0_Port->OTYPER &= ~(1UL << m_OutPositions[0]);	//Setting Column 1 high
		m_ActiveColumn = COL1;
	}
	else if(colNum == COL2)
	{
		//Set selected column, and make other columns floating
		m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
		m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
                m_KeypadStruct.OUT3_Port->OTYPER |= (1UL << m_OutPositions[3]);
		m_KeypadStruct.OUT1_Port->OTYPER &= ~(1UL << m_OutPositions[1]);	//Setting Column 2 high
		m_ActiveColumn = COL2;
	}
	else if(colNum == COL3)
	{
		//Set selected column, and make other columns floating
		m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
		m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
                m_KeypadStruct.OUT3_Port->OTYPER |= (1UL << m_OutPositions[3]);
		m_KeypadStruct.OUT2_Port->OTYPER &= ~(1UL << m_OutPositions[2]);	//Setting Column 3 high
		m_ActiveColumn = COL3;
	}
        else if(colNum == COL4)
	{
		//Set selected column, and make other columns floating
		m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
		m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
                m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
		m_KeypadStruct.OUT3_Port->OTYPER &= ~(1UL << m_OutPositions[3]);	//Setting Column 4 high
		m_ActiveColumn = COL4;
	}
	else
	{
		//Make all Columns floating.
                m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
		m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
		m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
                m_KeypadStruct.OUT3_Port->OTYPER |= (1UL << m_OutPositions[3]);
		m_ActiveColumn = NO_COL;
	}  
}