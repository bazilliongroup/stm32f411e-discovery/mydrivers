/*
Library:		                4x4 Keypad drive for STM32 MCUs
Written by:				Mohamed Yaqoob
Date written:				03/04/2018
Description:				The MY_Keypad4x4 library consists of the following public functions
					Function(1)- Keypad4x4_Init
					Function(2)- Keypad4x4_ReadKeypad
					Function(3)- Keypad4x4_GetChar
Modifications:
-----------------
Ronald Bazillion 	09/03/2018      Refactored original code 
*/

#ifndef __KEYPAD4X4
#define __KEYPAD4X4

//Header files
#include "stm32f4xx_hal.h"
#include <stdbool.h>

#define KEYS 16
#define NUM_OF_REG 16

//*******Enums*****************//

typedef enum KeyNum
{
	KEY1 = 0,
	KEY2 = 1,
	KEY3 = 2,
        KEY_A = 3,
	KEY4 = 4,
	KEY5 = 5,
	KEY6 = 6,
        KEY_B = 7,
	KEY7 = 8,
	KEY8 = 9,
	KEY9 = 10,
        KEY_C = 11,
	KEY_STAR = 12,
	KEY0 = 13,
	KEY_POUND = 14,
        KEY_D = 15,
        KBD_INVALID = -1
}KEYNUM;

typedef enum activeColumn
{
	COL1 = 1,
	COL2 = 2,
	COL3 = 3,
        COL4 = 4,
	NO_COL = -1
}ACTIVE_COLUMN;

//***** Constant variables and typedefs *****//
typedef struct
{
	GPIO_TypeDef* IN0_Port;         //row1 keypad pin8
	GPIO_TypeDef* IN1_Port;         //row2 keypad pin7
	GPIO_TypeDef* IN2_Port;         //row3 keypad pin6
	GPIO_TypeDef* IN3_Port;	        //row4 keypad pin5
        
	GPIO_TypeDef* OUT0_Port;	//col1 keypad pin4
	GPIO_TypeDef* OUT1_Port;	//col2 keypad pin3
	GPIO_TypeDef* OUT2_Port;	//col3 keypad pin2
	GPIO_TypeDef* OUT3_Port;	//col4 keypad pin1
	
	uint16_t	IN0pin;
	uint16_t	IN1pin;
	uint16_t	IN2pin;
	uint16_t	IN3pin;
        
	uint16_t	OUT0pin;
	uint16_t	OUT1pin;
	uint16_t	OUT2pin;
	uint16_t	OUT3pin;
}Keypad_WiresTypeDef;

//List of keys as chars

//***** Functions prototype *****//
//Function(1): Set Keypad pins and ports
void Keypad4x4_Init(Keypad_WiresTypeDef  *KeypadWiringStruct, bool rotate);
//Function(2): Read active keypad button
KEYNUM Keypad4x4_ReadKeypad(void);
//Function(3): Get character
char Keypad4x4_GetChar(KEYNUM key);
//Function(4): Get constant character
const char* Keypad4x4_GetConstChar(KEYNUM key);
#endif


