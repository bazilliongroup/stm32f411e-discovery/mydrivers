/*
Library:			LCD TFT EPD Driver for STM32 MCUs
Written by:			Ronald Bazillion
Date written:			10/08/2018
Description:			This is a 2.15  � a -Si, active matrix TFT, Electronic Paper Display (EPD) panel. The panel has such high resolution (110 dpi) 
                                that it is able to easily display fine patterns.	
Resources:

Modifications:
-----------------

*/

#ifndef __LCD_TFT_EPD_EPD215_H_
#define __LCD_TFT_EPD_EPD215_H_

//***** Header files *****//
#include <stdlib.h>
#include <stdbool.h>
#include "stm32f4xx_hal.h"

//***** Defines ********//
#define WIDTH    112
#define HEIGHT   208

#define BLACK    0
#define WHITE    1
#define INVERSE  2

#define MAX_SIZE ((WIDTH * HEIGHT) / 8) //2912 bytes per image

#define DRIVEROUTPUTCONTROL 0x01
#define GATESCANSTART       0x0F
#define DATAENTRYMODE       0x11
#define SETRAMXSTARTEND     0x44
#define SETRAMYSTARTEND     0x45
#define SETRAMXADDRESS      0x4E
#define SETRAMYADDRESS      0x4F
#define SETBORDERWAVEFORM   0x3C
#define SETGATEDRIVINGVOLT  0x03
#define NORMALANALOGMODE1   0x05
#define NORMALANALOGMODE2   0x75
#define SENDIMAGEDATA       0x24
#define SETTEMPERATURE      0x1A
#define SETUPDATESEQUENCE   0x22
#define ACTIVEDISPLAYUPDATE 0x20
  
//***** typedef enums *****//
typedef enum 
{
  TFT_BLANK,                    //
  TFT_EXCLAMATION,              // !
  TFT_DOUBLE_QUOTE,             // "
  TFT_POUND,                    // #
  TFT_DOLLAR,                   // $
  TFT_PERCENTAGE,               // %
  TFT_AND_PERCENT,              // &
  TFT_SINGLE_QUOTE,             // '
  TFT_BRACE_RIGHT,              // (
  TFT_BRACE_LEFT,               // )
  TFT_MULTIPLY,                 // *
  TFT_PLUS,                     // +
  TFT_COMMA,                    // ,
  TFT_MINUS,                    // -
  TFT_PERIOD,                   // .
  TFT_FORWARD_SLASH,            // /
  TFT_0,                        // 0
  TFT_1,                        // 1
  TFT_2,                        // 2
  TFT_3,                        // 3
  TFT_4,                        // 4
  TFT_5,                        // 5
  TFT_6,                        // 6
  TFT_7,                        // 7
  TFT_8,                        // 8
  TFT_9,                        // 9
  TFT_COLON,                    // :
  TFT_SEMICOLON,                // ;
  TFT_LESS_THAN,                // <
  TFT_EQUAL,                    // =
  TFT_GREATER_THAN,             // >
  TFT_QUESTION_MARK,            // ?
  TFT_AT_SIGN,                  // @
  TFT_A,                        // A
  TFT_B,                        // B
  TFT_C,                        // C
  TFT_D,                        // D
  TFT_E,                        // E
  TFT_F,                        // F
  TFT_G,                        // G
  TFT_H,                        // H
  TFT_I,                        // I
  TFT_J,                        // J
  TFT_K,                        // K
  TFT_L,                        // L
  TFT_M,                        // M
  TFT_N,                        // N
  TFT_O,                        // O
  TFT_P,                        // P
  TFT_Q,                        // Q
  TFT_R,                        // R
  TFT_S,                        // S
  TFT_T,                        // T
  TFT_U,                        // U
  TFT_V,                        // V
  TFT_W,                        // W
  TFT_X,                        // X
  TFT_Y,                        // Y
  TFT_Z,                        // Z
  TFT_BRACKET_RIGHT,            // [
  TFT_SPACE,                    //  
  TFT_BRACKET_LEFT,             // ]
  TFT_CARROT,                   // ^
  TFT_UNDERSCORE,               // _
  TFT_GRAVE,                    // `
  TFT_a,                        // a
  TFT_b,                        // b
  TFT_c,                        // c
  TFT_d,                        // d
  TFT_e,                        // e
  TFT_f,                        // f
  TFT_g,                        // g
  TFT_h,                        // h
  TFT_i,                        // i
  TFT_j,                        // j
  TFT_k,                        // k
  TFT_l,                        // l
  TFT_m,                        // m
  TFT_n,                        // n
  TFT_o,                        // o
  TFT_p,                        // p
  TFT_q,                        // q
  TFT_r,                        // r
  TFT_s,                        // s
  TFT_t,                        // t
  TFT_u,                        // u
  TFT_v,                        // v
  TFT_w,                        // w
  TFT_x,                        // x
  TFT_y,                        // y
  TFT_z,                        // z
  TFT_CURLY_BRACE_LEFT,         // {
  TFT_VERTICAL_BAR,             // |
  TFT_CURLY_BRACE_RIGHT,        // }
  TFT_TILDE,                    // ~
}SYMBOL_NAME;

//***** Constant variables and typedefs *****//
typedef struct TFT_WiresTypeDef
{
//  uint8_t* txSpiBuffer;    //SDIN_D1 Data (MOSI) - pointer to tx buffer.
  
//  GPIO_TypeDef* sclkDoPort;    //SCLK_D0 only used when not in SPI mode
//  uint16_t      sclkDoPin;
  
  GPIO_TypeDef* chipSelectPort;    //Chip Select (CS)
  uint16_t      chipSelectPin;
  
  GPIO_TypeDef* dataCommandPort;        //D/C# - (Data/Command) 1 for Data / 0 for Command
  uint16_t      dataCommandPin;
  
  GPIO_TypeDef* busySelectPort;        //bs - (Busy Select) used to check if busy flag is on (high) or not (low).
  uint16_t      busySelectPin;

  GPIO_TypeDef* panelOnPort;        //Panel On # 
  uint16_t      panelOnPin;
  
  GPIO_TypeDef* resetPort;          //rs - (Reset #) set high upon power on of COG driver RES# = 1
  uint16_t      resetPin;
  
}TFT_WIRES_TYPEDEF;

//***** Library variables *****//


//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//
void EPD215_init( SPI_HandleTypeDef* spiHdl, UART_HandleTypeDef* uartHdl, TFT_WIRES_TYPEDEF* tftWireDef);
void EPD215_powerCogDriver(void);
void EPD215_initCogDriver(void);
void EPD215_updateCommandSequence(void);
void EPD215_clearScreen(uint8_t color);
void EPD215_loadImage( uint8_t *buf, int len, bool invert );
void EPD215_sendTemperature(uint8_t temperature);
void EPD215_sendImage(void);
void EPD215_sendPattern( uint8_t pattern);
void EPD215_sendCharacter(const SYMBOL_NAME symbol);
void EPD215_drawPixel( int16_t x, int16_t y, uint16_t color );

#endif  /* __LCD_TFT_EPD_EPD215_H_ */
