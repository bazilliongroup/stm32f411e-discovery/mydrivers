/*
Library:				4x3 Keypad drive for STM32 MCUs
Written by:				Ronald Bazillion
Date written:				08/02/2018
Description:				The keypad4x3 library consists of the following public functions
                                        Function(1)- Keypad4x3_Init
                                        Function(2)- Keypad4x3_ReadKeypad
                                        Function(3)- Keypad4x3_GetChar

Modifications:
-----------------
Ronald Bazillion 	08/02/2018	Modified to a 4x3 original 4x4 STM driver code.
Ronald Bazillion 	09/05/2018	Refactored 4x3 code to make more flexible 
*/

#ifndef __KEYPAD4X3
#define __KEYPAD4X3

//Header files
#include "stm32f4xx_hal.h"
#include <stdbool.h>

#define KEYS 12
#define NUM_OF_REG 16

//*******Enums*****************//

typedef enum KeyNum
{
	KEY1 = 0,
	KEY2 = 1,
	KEY3 = 2,
	KEY4 = 3,
	KEY5 = 4,
	KEY6 = 5,
	KEY7 = 6,
	KEY8 = 7,
	KEY9 = 8,
	KEY_STAR = 9,
	KEY0 = 10,
	KEY_POUND = 11,
        KBD_INVALID = -1

}KEYNUM;
typedef enum activeColumn
{
	COL1 = 1,
	COL2 = 2,
	COL3 = 3,
	NO_COL = -1
}ACTIVE_COLUMN;

//***** Constant variables and typedefs *****//
typedef struct
{
	GPIO_TypeDef* IN0_Port;		//row1 keypad pin7
	GPIO_TypeDef* IN1_Port;		//row2 keypad pin6
	GPIO_TypeDef* IN2_Port;		//row3 keypad pin5
	GPIO_TypeDef* IN3_Port;		//row4 keypad pin4
	GPIO_TypeDef* OUT0_Port;	//col1 keypad pin3
	GPIO_TypeDef* OUT1_Port;	//col2 keypad pin2
	GPIO_TypeDef* OUT2_Port;	//col3 keypad pin1
	
	uint16_t	IN0pin;
	uint16_t	IN1pin;
	uint16_t	IN2pin;
	uint16_t	IN3pin;
	uint16_t	OUT0pin;
	uint16_t	OUT1pin;
	uint16_t	OUT2pin;
}Keypad_WiresTypeDef;

//List of keys as chars

//***** API Functions prototype *****//
//Function(1): Set Keypad pins and ports
void Keypad4x3_Init(Keypad_WiresTypeDef  *KeypadWiringStruct, bool rotate);
//Function(2): Read active keypad button
KEYNUM Keypad4x3_ReadKeypad(void);
//Function(3): Get character
char Keypad4x3_GetChar(KEYNUM key);

#endif
