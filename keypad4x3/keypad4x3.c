/*
Library:				4x3 Keypad drive for STM32 MCUs
Written by:				Ronald Bazillion
Date written:				08/02/2018
Description:				The keypad4x3 library consists of the following public functions
                                        Function(1)- Keypad4x3_Init
                                        Function(2)- Keypad4x3_ReadKeypad
                                        Function(3)- Keypad4x3_GetChar

                                        The 4X3 represents 4 rows X 3 columns modified for the 4x3 Matrix Membrane Keypad
                                        I recommend you build it manually and do the modifications. Here is the logic matrix.

                                        PINS (7-4)rows; (3-1)cols.: Pins 7,6,5,4 should have a pull down resistor, I used 4.7K Ohms
                                        row1 = pin7, row2 = pin6, row3 = pin5, row4 = pin4
                                        col1 = pin3, col2 = pin2, col3 = pin1

                                       |  rows |cols |
                                       |7,6,5,4|3,2,1| <- PINS
                                       --------|-------------------
                                       [1,2,3,4|1,2,3] <- ROW/COL
                                       ----------------------------
                                        1,0,0,0,1,0,0	  '1'
                                        1,0,0,0,0,1,0	  '2'
                                        1,0,0,0,0,0,1	  '3'
                                        0,1,0,0,1,0,0	  '4'
                                        0,1,0,0,0,1,0	  '5'
                                        0,1,0,0,0,0,1	  '6'
                                        0,0,1,0,1,0,0	  '7'
                                        0,0,1,0,0,1,0	  '8'
                                        0,0,1,0,0,0,1	  '9'
                                        0,0,0,1,1,0,0	  '*'
                                        0,0,0,1,0,1,0	  '0'
                                        0,0,0,1,0,0,1	  '#'

                                        The way the keypad algorithm works is you set a column high and check each row to see if a key was pressed. For example if number 3 is pressed
                                        col3 will high high and so will row1. Each number/letter has a code of a combination of rows and columns. This code goes from left to right 
                                        with rows and columns. So 3 is row1/col3.
Resources:
                                        https://www.jameco.com/webapp/wcs/stores/servlet/ProductDisplay?langId=-1&storeId=10001&productId=2246338&catalogId=10001&CID=CATJAN218PDF
                                        https://www.youtube.com/watch?v=vcxz2Dhx5WY&list=PLfExI9i0v1sn_lQjCFJHrDSpvZ8F2CpkA&index=23
                                        http://hertaville.com/stm32f0-gpio-tutorial-part-1.html

Modifications:
-----------------
Ronald Bazillion 	08/02/2018	Modified to a 4x3 original 4x4 STM driver code.
Ronald Bazillion 	09/05/2018	Refactored 4x3 code to make more flexible 

*/

//***** Header files *****//
#include "stm32f7xx_hal.h"
#include "keypad4x3.h"

//*******Defines***************//
//#define KEYPAD_TEST

//*******Enums*****************//

//***** Constant variables and typedefs *****//

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//
//1. Keypad pin out variable (keypad library)
static Keypad_WiresTypeDef m_KeypadStruct;
//2. OUT pins position, or pin number in decimal for use in column change function
static uint8_t m_OutPositions[3];
//
//static char *Keypad_keys[KEYS] =
//{
//	"1",
//	"2",
//	"3",
//	"4",
//	"5",
//	"6",
//	"7",
//	"8",
//	"9",
//	"*",
//	"0",
//	"#",
//};

static ACTIVE_COLUMN m_ActiveColumn;                    //variable to store active column when searching.
static bool m_RotateRowsCols;                           //option used in case the 4x4 keypad matrix is reversed.

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//

//Function(1): Change column number
static void m_Keypad4x3_ChangeColumn(ACTIVE_COLUMN colNum);

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

//Function(1): Set Keypad pins and ports, rotate is used in case the keypad matrix is reversed.
//The defeault setting "false" has (rows & columns) while "true" has (colums & rows). 
void Keypad4x3_Init(Keypad_WiresTypeDef  *KeypadWiringStruct, bool rotate)
{
	m_ActiveColumn = NO_COL;

	//Step(1): Copy the Keypad wirings to the library
	m_KeypadStruct = *KeypadWiringStruct;

	//Step(2): Find the positions of the 3 OUT pins
	//Scroll through OUTXpin value and record the position.
	//EX. OUT0pin = GPIO_PIN_1 = ((uint16_t)0x0002) scroll until the bit is found and record as idx
	for(uint8_t idx = 0; idx < NUM_OF_REG; idx++)
	{
		//find the position
		if(((m_KeypadStruct.OUT0pin>>idx)&0x0001) == 0x0001)
		{
			m_OutPositions[0] = idx;
		}

		if(((m_KeypadStruct.OUT1pin>>idx)&0x0001) == 0x0001)
		{
			m_OutPositions[1] = idx;
		}

		if(((m_KeypadStruct.OUT2pin>>idx)&0x0001) == 0x0001)
		{
			m_OutPositions[2] = idx;
		}
	}

	//Step(3): Initialize all pins to set all OUT pins to "Open Drain" and set pins to high.
	m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
	m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
	m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);

	HAL_GPIO_WritePin(m_KeypadStruct.OUT0_Port, m_KeypadStruct.OUT0pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(m_KeypadStruct.OUT1_Port, m_KeypadStruct.OUT1pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(m_KeypadStruct.OUT2_Port, m_KeypadStruct.OUT2pin, GPIO_PIN_SET);
        
        #ifdef KEYPAD_TEST
        m_KeypadStruct.OUT0_Port->OTYPER &= ~(1UL << m_OutPositions[0]);	//Setting Column 1 high
        m_KeypadStruct.OUT1_Port->OTYPER &= ~(1UL << m_OutPositions[1]);	//Setting Column 2 high
        m_KeypadStruct.OUT2_Port->OTYPER &= ~(1UL << m_OutPositions[2]);	//Setting Column 3 high
#endif   
}

//Function(2): Read active keypad button
//NOTE: with this function the Row ports Pins7-4 are configured to be resistor pull down.
//      This can be done in the software or done with actual pull down resistors.
//      I originally had it not returning but with arduino keypads it returned false row values
//      so I return upon the first keypress found from this function. Through testing it worked fine.
KEYNUM Keypad4x3_ReadKeypad(void)
{
	//Step(1): Make Pin3 High and check the rows
	m_Keypad4x3_ChangeColumn(COL1);
        if (m_ActiveColumn == COL1)
        {
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN0_Port, m_KeypadStruct.IN0pin) == GPIO_PIN_SET) { return KEY1; }	//Pin3 and Pin7
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN1_Port, m_KeypadStruct.IN1pin) == GPIO_PIN_SET) { return KEY4; }	//Pin3 and Pin6
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN2_Port, m_KeypadStruct.IN2pin) == GPIO_PIN_SET) { return KEY7; }	//Pin3 and Row5
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN3_Port, m_KeypadStruct.IN3pin) == GPIO_PIN_SET) { return KEY_STAR; } 	//Pin3 and Row4
        }
          
	//Step(2): Make Pin2 High and check the rows
	m_Keypad4x3_ChangeColumn(COL2);
        if (m_ActiveColumn == COL2)
        {
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN0_Port, m_KeypadStruct.IN0pin) == GPIO_PIN_SET) { return KEY2; } //Pin2 and Pin7
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN1_Port, m_KeypadStruct.IN1pin) == GPIO_PIN_SET) { return KEY5; } //Pin2 and Pin6
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN2_Port, m_KeypadStruct.IN2pin) == GPIO_PIN_SET) { return KEY8; } //Pin2 and Pin5
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN3_Port, m_KeypadStruct.IN3pin) == GPIO_PIN_SET) { return KEY0; } //Pin2 and Pin5
        }
                
	//Step(3): Make Pin3 High and check the rows
	m_Keypad4x3_ChangeColumn(COL3);
        if (m_ActiveColumn == COL3)
        {
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN0_Port, m_KeypadStruct.IN0pin) == GPIO_PIN_SET) { return KEY3; }      //Pin1 and Pin7
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN1_Port, m_KeypadStruct.IN1pin) == GPIO_PIN_SET) { return KEY6; }      //Pin1 and Pin6
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN2_Port, m_KeypadStruct.IN2pin) == GPIO_PIN_SET) { return KEY9; }      //Pin1 and Pin5
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN3_Port, m_KeypadStruct.IN3pin) == GPIO_PIN_SET) { return KEY_POUND; } //Pin1 and Pin4
        }       
                
        //Revert cols back to default
        m_Keypad4x3_ChangeColumn(NO_COL);
	return KBD_INVALID;
}

//Function(3): Get character
char Keypad4x3_GetChar(KEYNUM key)
{
	switch (key)
	{
	case KEY1:
          if (m_RotateRowsCols) { return '1'; }
          else { return '1'; }
	  break;
	case KEY2:
          if (m_RotateRowsCols) { return '4';}
          else { return '2'; }
          break;
	case KEY3:
          if (m_RotateRowsCols) { return  '7';}
	  else { return '3'; }
          break;
	case KEY4:
          if (m_RotateRowsCols) { return '2'; }
	  else { return '4'; }
	  break;
	case KEY5:
          if (m_RotateRowsCols) { return '5'; }
	  else { return '5'; }
	  break;
	case KEY6:
          if (m_RotateRowsCols) { return '8'; }
	  else { return '6'; }
	  break;
	case KEY7:
          if (m_RotateRowsCols) { return '3'; } 
	  else { return '7'; }
	  break;
	case KEY8:
          if (m_RotateRowsCols) { return '6'; }
	  else { return '8'; }
	  break;
	case KEY9:
          if (m_RotateRowsCols) { return '9'; }
	  else { return '9'; }
	  break;
	case KEY_STAR:
          if (m_RotateRowsCols) { return 'A'; }
	  else { return '*'; }
	  break;
	case KEY0:
          if (m_RotateRowsCols) { return 'B'; }
	  else { return '0'; }
	  break;
	case KEY_POUND:
          if (m_RotateRowsCols) { return 'C'; }
          else { return '#'; }
	  break;
	}

	return NULL;
}

//Function(4): Change column number.
//It does this by changing the state of the type register from open-drain to Output Push Pull.
static void m_Keypad4x3_ChangeColumn(ACTIVE_COLUMN colNum)
{
	if(colNum == COL1)
	{
		//Set selected column, and make other columns floating
		m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
		m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
		m_KeypadStruct.OUT0_Port->OTYPER &= ~(1UL << m_OutPositions[0]);	//Setting Column 1 high
		m_ActiveColumn = COL1;
	}
	else if(colNum == COL2)
	{
		//Set selected column, and make other columns floating
		m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
		m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
		m_KeypadStruct.OUT1_Port->OTYPER &= ~(1UL << m_OutPositions[1]);	//Setting Column 2 high
		m_ActiveColumn = COL2;
	}
	else if(colNum == COL3)
	{
		//Set selected column, and make other columns floating
		m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
		m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
		m_KeypadStruct.OUT2_Port->OTYPER &= ~(1UL << m_OutPositions[2]);	//Setting Column 3 high
		m_ActiveColumn = COL3;
	}
	else
	{
		//Make all Columns floating.
                m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
		m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
		m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
		m_ActiveColumn = NO_COL;
	}
}
