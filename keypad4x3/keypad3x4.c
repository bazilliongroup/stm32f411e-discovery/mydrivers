/*
Library:				3x4 Keypad drive for STM32 MCUs
Written by:				Mohamed Yaqoob
Date written:				03/04/2018
Description:				The keypad3x4 library consists of the following public functions
                                        Function(1)- Keypad3x4_Init
                                        Function(2)- Keypad3x4_ReadKeypad
                                        Function(3)- Keypad3x4_GetChar

                                        The 3X4 represents 3 columns X 4 rows modified for the 3x4 Matrix Membrane Keypad
                                        I recommend you build it manually and do the modifications. Here is the logic matrix.

                                        PINS (7-5)rows; (3-1)cols.: Pins 7,6,5,4 should have a pull down resistor, I used 4.7K Ohms
                                        7,6,5,4,3,2,1
                                        -------------
                                        1,0,0,0,1,0,0	'1'
                                        1,0,0,0,0,1,0	'2'
                                        1,0,0,0,0,0,1	'3'
                                        0,1,0,0,1,0,0	'4'
                                        0,1,0,0,0,1,0	'5'
                                        0,1,0,0,0,0,1	'6'
                                        0,0,1,0,1,0,0	'7'
                                        0,0,1,0,0,1,0	'8'
                                        0,0,1,0,0,0,1	'9'
                                        0,0,0,1,1,0,0	'*'
                                        0,0,0,1,0,1,0	'0'
                                        0,0,0,1,0,0,1	'#'
Resources:
                                        https://www.jameco.com/webapp/wcs/stores/servlet/ProductDisplay?langId=-1&storeId=10001&productId=2246338&catalogId=10001&CID=CATJAN218PDF
                                        https://www.youtube.com/watch?v=vcxz2Dhx5WY&list=PLfExI9i0v1sn_lQjCFJHrDSpvZ8F2CpkA&index=23
                                        http://hertaville.com/stm32f0-gpio-tutorial-part-1.html

Modifications:
-----------------
Ronald Bazillion 	08/02/2018	Modified to a 3x4 STM driver code.

*/

//***** Header files *****//
#include "stm32f7xx_hal.h"
#include "keypad3x4.h"

//*******Enums*****************//

//***** Constant variables and typedefs *****//

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//
//1. Keypad pin out variable (keypad library)
static Keypad_WiresTypeDef m_KeypadStruct;
//2. OUT pins position, or pin number in decimal for use in column change function
static uint8_t m_OutPositions[3];
//
static char *Keypad_keys[KEYS] =
{
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"*",
	"0",
	"#",
};

static ACTIVE_COLUMN activeColumn;

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//

//Function(1): Change column number
static void m_Keypad3x4_ChangeColumn(ACTIVE_COLUMN colNum);

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

//Function(1): Set Keypad pins and ports
void Keypad3x4_Init(Keypad_WiresTypeDef  *KeypadWiringStruct)
{
	activeColumn = NO_COL;

	//Step(1): Copy the Keypad wirings to the library
	m_KeypadStruct = *KeypadWiringStruct;

	//Step(2): Find the positions of the 3 OUT pins
	//Scroll through OUTXpin value and record the position.
	//EX. OUT0pin = GPIO_PIN_1 = ((uint16_t)0x0002) scroll until the bit is found and record as idx
	for(uint8_t idx = 0; idx < NUM_OF_REG; idx++)
	{
		//find the position
		if(((m_KeypadStruct.OUT0pin>>idx)&0x0001) == 0x0001)
		{
			m_OutPositions[0] = idx;
		}

		if(((m_KeypadStruct.OUT1pin>>idx)&0x0001) == 0x0001)
		{
			m_OutPositions[1] = idx;
		}

		if(((m_KeypadStruct.OUT2pin>>idx)&0x0001) == 0x0001)
		{
			m_OutPositions[2] = idx;
		}
	}

	//Step(3): Initialize all pins to set all OUT pins to "Open Drain" and set pins to high.
	m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
	m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
	m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);

	HAL_GPIO_WritePin(m_KeypadStruct.OUT0_Port, m_KeypadStruct.OUT0pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(m_KeypadStruct.OUT1_Port, m_KeypadStruct.OUT1pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(m_KeypadStruct.OUT2_Port, m_KeypadStruct.OUT2pin, GPIO_PIN_SET);
}

//Function(2): Read active keypad button
//NOTE: with this function the Row ports Pins7-4 are configured to be resistor pull down.
//      This can be done in the software or done with actual pull down resistors.
//      I originally had it not returning but with arduino keypads it returned false row values
//      so I return upon the first keypress found from this function. Through testing it worked fine.
KEYNUM Keypad3x4_ReadKeypad(void)
{
	//Step(1): Make Pin3 High and check the rows
	m_Keypad3x4_ChangeColumn(COL1);
        if (activeColumn == COL1)
        {
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN0_Port, m_KeypadStruct.IN0pin) == GPIO_PIN_SET) { return KEY1; }	//Pin3 and Pin7
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN1_Port, m_KeypadStruct.IN1pin) == GPIO_PIN_SET) { return KEY4; }	//Pin3 and Pin6
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN2_Port, m_KeypadStruct.IN2pin) == GPIO_PIN_SET) { return KEY7; }	//Pin3 and Row5
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN3_Port, m_KeypadStruct.IN3pin) == GPIO_PIN_SET) { return KEY_STAR; } 	//Pin3 and Row4
        }
          
	//Step(2): Make Pin2 High and check the rows
	m_Keypad3x4_ChangeColumn(COL2);
        if (activeColumn == COL2)
        {
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN0_Port, m_KeypadStruct.IN0pin) == GPIO_PIN_SET) { return KEY2; } //Pin2 and Pin7
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN1_Port, m_KeypadStruct.IN1pin) == GPIO_PIN_SET) { return KEY5; } //Pin2 and Pin6
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN2_Port, m_KeypadStruct.IN2pin) == GPIO_PIN_SET) { return KEY8; } //Pin2 and Pin5
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN3_Port, m_KeypadStruct.IN3pin) == GPIO_PIN_SET) { return KEY0; } //Pin2 and Pin5
        }
                
	//Step(3): Make Pin3 High and check the rows
	m_Keypad3x4_ChangeColumn(COL3);
        if (activeColumn == COL3)
        {
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN0_Port, m_KeypadStruct.IN0pin) == GPIO_PIN_SET) { return KEY3; }      //Pin1 and Pin7
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN1_Port, m_KeypadStruct.IN1pin) == GPIO_PIN_SET) { return KEY6; }      //Pin1 and Pin6
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN2_Port, m_KeypadStruct.IN2pin) == GPIO_PIN_SET) { return KEY9; }      //Pin1 and Pin5
            if (HAL_GPIO_ReadPin(m_KeypadStruct.IN3_Port, m_KeypadStruct.IN3pin) == GPIO_PIN_SET) { return KEY_POUND; } //Pin1 and Pin4
        }       
                
        //Revert cols back to default
        m_Keypad3x4_ChangeColumn(NO_COL);
	return KBD_INVALID;
}

//Function(3): Get character
char* Keypad3x4_GetChar(KEYNUM key)
{
	uint8_t keypadSw = 0;
	switch (key)
	{
	case KEY1:
		keypadSw = 0;
		break;
	case KEY2:
		keypadSw = 1;
		break;
	case KEY3:
		keypadSw = 2;
		break;
	case KEY4:
		keypadSw = 3;
		break;
	case KEY5:
		keypadSw = 4;
		break;
	case KEY6:
		keypadSw = 5;
		break;
	case KEY7:
		keypadSw = 6;
		break;
	case KEY8:
		keypadSw = 7;
		break;
	case KEY9:
		keypadSw = 8;
		break;
	case KEY_STAR:
		keypadSw = 9;
		break;
	case KEY0:
		keypadSw = 10;
		break;
	case KEY_POUND:
		keypadSw = 11;
		break;
	default:
		return NULL;
	}

	return Keypad_keys[keypadSw];
}

//Function(4): Change column number.
//It does this by changing the state of the type register from open-drain to Output Push Pull.
static void m_Keypad3x4_ChangeColumn(ACTIVE_COLUMN colNum)
{
	if(colNum == COL1)
	{
		//Set selected column, and make other columns floating
		m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
		m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
		m_KeypadStruct.OUT0_Port->OTYPER &= ~(1UL << m_OutPositions[0]);	//Setting Column 1 high
		activeColumn = COL1;
	}
	else if(colNum == COL2)
	{
		//Set selected column, and make other columns floating
		m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
		m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
		m_KeypadStruct.OUT1_Port->OTYPER &= ~(1UL << m_OutPositions[1]);	//Setting Column 2 high
		activeColumn = COL2;
	}
	else if(colNum == COL3)
	{
		//Set selected column, and make other columns floating
		m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
		m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
		m_KeypadStruct.OUT2_Port->OTYPER &= ~(1UL << m_OutPositions[2]);	//Setting Column 3 high
		activeColumn = COL3;
	}
	else
	{
		//Make all Columns floating.
                m_KeypadStruct.OUT0_Port->OTYPER |= (1UL << m_OutPositions[0]);
		m_KeypadStruct.OUT1_Port->OTYPER |= (1UL << m_OutPositions[1]);
		m_KeypadStruct.OUT2_Port->OTYPER |= (1UL << m_OutPositions[2]);
		activeColumn = NO_COL;
	}
}
